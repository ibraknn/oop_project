package services;

import domain.models.Student;
import repositories.entities.StudentRepository;
import repositories.interfaces.IStudentRepository;
import services.interfaces.IStudentService;

import java.util.List;

public class StudentService implements IStudentService {
    private IStudentRepository studentRepo;

    public StudentService(){
        studentRepo = new StudentRepository();
    }

    @Override
    public Student getStudentByID(int id){
        return studentRepo.getStudentByID(id);
    }

    @Override
    public List<Student> getAll(){
        return studentRepo.getAll();
    }

    @Override
    public long getNumber(){
        return studentRepo.getNumber();
    }

    @Override
    public void addStudent(Student student){
        studentRepo.add(student);
    }
}
