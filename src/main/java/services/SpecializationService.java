package services;

import repositories.entities.SpecializationRepository;
import repositories.interfaces.ISpecializationRepository;
import services.interfaces.ISpecializationService;

public class SpecializationService implements ISpecializationService {
    private ISpecializationRepository specRepo;

    public SpecializationService() {
        specRepo = new SpecializationRepository();
    }

    @Override
    public void addSpecialization(String name) {
        specRepo.addSpecialization(name);
    }
}
