import controllers.GroupController;
import controllers.SpecializationController;
import controllers.StudentController;
import controllers.SubjectController;
import filters.CorsFilter;
import filters.SecurityFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api/university")
public class MyApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> hs = new HashSet<>();
        hs.add(MultiPartFeature.class);

        hs.add(GroupController.class);
        hs.add(SpecializationController.class);
        hs.add(SubjectController.class);
        
        hs.add(StudentController.class);

        hs.add(CorsFilter.class);
        hs.add(SecurityFilter.class);
        return hs;
    }
}