package domain.models;

import java.util.LinkedList;
import java.util.List;

public class Group {
    private int id;
    private int name;
    private Specialization spec;
    private List<Student> students;

    public Group(int name) {
        setName(name);
        students = new LinkedList<>();
    }

    public Group(int id, int name) {
        this(name);
        setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public Specialization getSpec() {
        return spec;
    }

    public void setSpec(Specialization spec) {
        this.spec = spec;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student){
        students.add(student);
    }
}
