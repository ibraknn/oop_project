package domain.models;

public class Specialization {
    private Integer id;
    private String name;

    public Specialization(int id, String name) {
        setId(id);
        setName(name);
    }

    public Specialization(String name) {
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
