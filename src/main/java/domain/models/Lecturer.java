package domain.models;

import java.sql.Date;

public class Lecturer extends Person {
    private String subject;
    private String degree;
    private int salary;
    private String office;

    public Lecturer(int id, String name, String surname, Date birthday, String subject, String degree, int salary, String office) {
        super(id, name, surname, birthday);
        setSubject(subject);
        setDegree(degree);
        setSalary(salary);
        setOffice(office);
    }

    public Lecturer(String name, String surname, Date birthday, String subject, String degree, int salary, String office) {
        super(name, surname, birthday);
        setSubject(subject);
        setDegree(degree);
        setSalary(salary);
        setOffice(office);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}
