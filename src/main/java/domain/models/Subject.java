package domain.models;

public class Subject {
    private int id;
    private String name;
    private int credits;

    public Subject(){}

    public Subject(String name, int credits) {
        setName(name);
        setCredits(credits);
    }

    public Subject(int id, String name, int credits) {
        setId(id);
        setName(name);
        setCredits(credits);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}
