package domain.models;

import java.sql.Date;

public class Student extends Person {
    private Specialization specialization;
    private int group;
    private String major;
    private float gpa;

    public Student(int id, String name, String surname, Date birthday, String major, float gpa) {
        super(id, name, surname, birthday);
        setMajor(major);
        setGpa(gpa);
    }

    public Student(int id, String name, String surname, Date birthday, Specialization specialization,
                   int group, String major, float gpa) {
        super(id, name, surname, birthday);
        setSpecialization(specialization);
        setGroup(group);
        setMajor(major);
        setGpa(gpa);
    }

    public Student(String name, String surname, Date birthday, int specialization_id,
                   int group, String major, float gpa) {
        super(name, surname, birthday);
        specialization.setId(specialization_id);
        setGroup(group);
        setMajor(major);
        setGpa(gpa);
    }

    public Student(String name, String surname, Date birthday, Specialization specialization,
                   int group, String major, float gpa) {
        super(name, surname, birthday);
        setSpecialization(specialization);
        setGroup(group);
        setMajor(major);
        setGpa(gpa);
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }
}
