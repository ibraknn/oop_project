package repositories.interfaces;

import domain.models.Student;

import java.util.List;

public interface IStudentRepository extends IEntityRepository<Student> {
    Student getStudentByID(int id);
    List<Student> getAll();
    long getNumber();
}
