package repositories.interfaces;

import domain.models.Specialization;

public interface ISpecializationRepository{
    void addSpecialization(String name);
    Specialization getSpecByName(String name);
    Specialization getSpecByID(int id);
}
