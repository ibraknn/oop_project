package repositories.entities;

import domain.models.Group;
import domain.models.Student;
import repositories.db.PostgresRepository;
import repositories.interfaces.IGroupRepository;

import javax.ws.rs.BadRequestException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupRepository implements IGroupRepository {

    private Connection postgres;

    public GroupRepository() {
        postgres = PostgresRepository.getConnection();
    }

    public Group groupInfo(int spec, int groupName) {
        try {
            Group group = getGroupByName(groupName);
            SpecializationRepository sRepo = new SpecializationRepository();
            group.setSpec(sRepo.getSpecByID(spec));
            String sql = "SELECT s.id, s.name,s.surname,s.birthday,s.major,s.gpa " +
                    "FROM students s " +
                    "WHERE specialization_id = ? AND group_id = (SELECT group_id FROM groupnum WHERE group_name = ?)";

            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setInt(1, spec);
            stmt.setInt(2, groupName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Student student = new Student(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getDate("birthday"),
                        rs.getString("major"),
                        rs.getFloat("gpa")
                );
                group.addStudent(student);
            }
            return group;
        } catch (SQLException e) {
            throw new BadRequestException();
        }
    }

    @Override
    public void addGroup(int name) {
        try {
            String sql = "INSERT INTO groupnum(group_name) VALUES ( ? )";
            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setInt(1, name);
            stmt.execute();
        } catch (SQLException e) {
            throw new BadRequestException();
        }
    }

    @Override
    public Group getGroupByName(int name) {
        try {
            String sql = "SELECT * FROM groupnum WHERE group_name = ?";
            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setInt(1, name);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) return new Group(rs.getInt("group_id"),
                                           rs.getInt("group_name"));
        } catch (SQLException e) {
            throw new BadRequestException();
        }
        return null;
    }
}
