package repositories.entities;

import domain.models.Specialization;
import domain.models.Student;
import repositories.db.PostgresRepository;
import repositories.interfaces.ISpecializationRepository;
import repositories.interfaces.IStudentRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class StudentRepository implements IStudentRepository {

    private Connection postgres;

    public StudentRepository() {
        postgres = PostgresRepository.getConnection();
    }

    @Override
    public Student getStudentByID(int id) {
        String sql = "SELECT s.id, s.name, s.surname, s.birthday, spec.specialization_name, gn.group_name, " +
                "s.major, s.gpa " +
                "FROM students s " +
                "INNER JOIN specialization spec " +
                "ON s.specialization_id = spec.specialization_id " +
                "INNER JOIN groupnum gn " +
                "ON s.group_id = gn.group_id " +
                "WHERE s.id = " + id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public List<Student> getAll() {
        String sql = "SELECT s.id, s.name, s.surname, s.birthday, spec.specialization_name, " +
                "gn.group_name, s.major, s.gpa " +
                "FROM students s " +
                "INNER JOIN specialization spec " +
                "ON s.specialization_id = spec.specialization_id " +
                "INNER JOIN groupnum gn " +
                "ON s.group_id = gn.group_id ";
        return query(sql);
    }

    @Override
    public long getNumber() {
        try {
            String sql = "SELECT COUNT(*) as number_of_students FROM students";
            ResultSet rs = postgres.createStatement().executeQuery(sql);
            if (rs.next())
                return rs.getLong("number_of_students");
        } catch (SQLException e) {
            throw new BadRequestException();
        }
        return 0;
    }

    @Override
    public void add(Student student) {
        try {
            String sql = "INSERT INTO students(" +
                    "name, surname, birthday, specialization_id, group_id, major, gpa) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setString(1, student.getName());
            stmt.setString(2, student.getSurname());
            stmt.setDate(3, student.getBirthday());
            stmt.setInt(4, student.getSpecialization().getId());
            stmt.setInt(5, student.getGroup());
            stmt.setString(6, student.getMajor());
            stmt.setFloat(7, student.getGpa());
            stmt.execute();
        } catch (SQLException e) {
            throw new BadRequestException();
        }
    }

    @Override
    public void remove(Student student) {

    }

    @Override
    public void update(Student student) {

    }

    @Override
    public Student queryOne(String sql) {
        try {
            Statement stmt = postgres.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                SpecializationRepository specRepo = new SpecializationRepository();
                Specialization specialization = specRepo.getSpecByName(rs.getString("specialization_name"));
                return new Student(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getDate("birthday"),
                        specialization,
                        rs.getInt("group_name"),
                        rs.getString("major"),
                        rs.getFloat("gpa")
                );
            }
        } catch (SQLException e) {
            throw new BadRequestException("Cannot run SQL statement " + e.getSQLState());
        }
        return null;
    }

    @Override
    public List<Student> query(String sql) {
        try {
            Statement stmt = postgres.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Student> students = new LinkedList<>();
            SpecializationRepository specRepo = new SpecializationRepository();
            while (rs.next()) {
                Specialization specialization = specRepo.getSpecByName(rs.getString("specialization_name"));
                students.add(new Student(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getDate("birthday"),
                        specialization,
                        rs.getInt("group_name"),
                        rs.getString("major"),
                        rs.getFloat("gpa")
                ));
            }
            return students;
        } catch (SQLException e) {
            throw new BadRequestException();
        }
    }
}
