package repositories.entities;

import domain.models.Specialization;
import repositories.db.PostgresRepository;
import repositories.interfaces.ISpecializationRepository;

import javax.ws.rs.BadRequestException;
import java.sql.*;

public class SpecializationRepository implements ISpecializationRepository {

    private Connection postgres;

    public SpecializationRepository() {
        postgres = PostgresRepository.getConnection();
    }

    @Override
    public void addSpecialization(String name) {
        try {
            String sql = "INSERT INTO specialization(specialization_name) VALUES ( ? )";
            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.execute();
        } catch (SQLException e) {
            throw new BadRequestException();
        }
    }

    @Override
    public Specialization getSpecByName(String name) {
        try {
            String sql = "SELECT * FROM specialization WHERE specialization_name = ? LIMIT 1";
            PreparedStatement stmt = postgres.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Specialization(
                        rs.getInt("specialization_id"),
                        rs.getString("specialization_name")
                );
            }
        } catch (SQLException e) {
            throw new BadRequestException("Cannot run SQL statement " + e.getSQLState());
        }
        return null;
    }

    @Override
    public Specialization getSpecByID(int id) {
        try {
            String sql = "SELECT * FROM specialization WHERE specialization_id = " + id + " LIMIT 1";
            Statement stmt = postgres.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Specialization(
                        rs.getInt("specialization_id"),
                        rs.getString("specialization_name")
                );
            }
        } catch (SQLException e) {
            throw new BadRequestException("Cannot run SQL statement " + e.getSQLState());
        }
        return null;
    }
}
