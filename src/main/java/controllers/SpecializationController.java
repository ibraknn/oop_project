package controllers;

import org.glassfish.jersey.media.multipart.FormDataParam;
import services.SpecializationService;
import services.interfaces.ISpecializationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/specialization")
public class SpecializationController {
    private ISpecializationService specService;

    public SpecializationController() {
        specService = new SpecializationService();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addSepcialization(@FormDataParam("name") String name) {
        try {
            specService.addSpecialization(name);
        } catch (ServerErrorException e) {
            return Response.serverError().build();
        } catch (BadRequestException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.CREATED).entity("Specialization successfully created!").build();
    }
}
