package controllers;

import domain.models.Group;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.GroupService;
import services.interfaces.IGroupService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/group")
public class GroupController {
    private IGroupService groupService;

    public GroupController() {
        groupService = new GroupService();
    }

    @GET
    @Path("/{specialization}/{group}")
    public Response getInfo(@PathParam("specialization") int spec,
                            @PathParam("group") int groupName) {
        Group group;
        try {
            group = groupService.groupInfo(spec, groupName);
        } catch (ServerErrorException e) {
            return Response
                    .serverError()
                    .build();
        } catch (BadRequestException e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .build();
        }
        if (group == null) return Response
                .status(Response.Status.NOT_FOUND)
                .build();
        else return Response
                .ok(group)
                .build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addGroup(@FormDataParam("name") int name) {
        try {
            groupService.addGroup(name);
        } catch (ServerErrorException e) {
            return Response
                    .serverError()
                    .build();
        } catch (BadRequestException e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .build();
        }
        return Response
                .status(Response.Status.CREATED)
                .entity("Group successfully created!")
                .build();
    }
}
